import Banner from '../components/Banner'
import Highlights from '../components/Highlights'
import FeaturedProducts from '../components/FeaturedProducts'


export default function Home() {


const data = {
    title: "Beauté Rendezvous",
    content: "Beauty and Wellness Hub",
    destination: "/products",
    label: "Add to cart now!"
}

	return (
		<>
			<Banner data={data} />
			<FeaturedProducts />
			<Highlights />

		</>
		
	)
}