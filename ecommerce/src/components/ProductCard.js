import { useState } from 'react' //purpose of useState (which is a hook) is to create a new state
import { Card, Button } from "react-bootstrap";
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom';


export default function ProductCard({productProp}) {

const {_id, name, description, price} = productProp;
console.log(useState(0))


  return (
    <Card className="mt-3">
      <Card.Body>
        <Card.Title>{name}</Card.Title>
        <Card.Subtitle>Description:</Card.Subtitle>
        <Card.Text>{description}</Card.Text>
        <Card.Subtitle>Price:</Card.Subtitle>
        <Card.Text>{price}</Card.Text>
        <Link className="btn btn-primary" to={`/products/${_id}`}>Add to Cart</Link>
      </Card.Body>
    </Card>
  )
}


// Check if the ProductCard component is getting the correct prop types
ProductCard.propTypes = {
  product: PropTypes.shape({
    name: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    price: PropTypes.number.isRequired
  })
}