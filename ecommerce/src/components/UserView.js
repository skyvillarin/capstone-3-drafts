import { useState, useEffect } from 'react';
import ProductCard from './ProductCard';
import ProductSearch from './ProductSearch';
import SearchByPrice from './SearchByPrice';


export default function UserView({productsData}) {
    return(
        <>
            <ProductSearch/>
            <SearchByPrice/>
            { productsData.length && (
                productsData.map(product => {
                    //only render the active products since the route used is /all from Products.js page
                    if(product.isActive) {
                        return <ProductCard productProp={product} key={product._id}/>
                    }
                })
            )}
        </>
        )
}
